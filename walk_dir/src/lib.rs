pub fn iterate_directory_gathering_filenames(dir: &std::path::Path, mut file_pathbuf_vector: std::vec::Vec<std::path::PathBuf>) -> Result<std::vec::Vec<std::path::PathBuf>, String>{
    // https://doc.rust-lang.org/std/fs/struct.ReadDir.html
    let path_iterator = std::fs::read_dir(dir).map_err(|err| {
        return format!("read_dir failed on path {} - {:?}",dir.display(),err);
    })?;
    
    // https://doc.rust-lang.org/std/fs/struct.DirEntry.html
    for dir_entry_res in path_iterator {
        let dir_entry = dir_entry_res.map_err(|err| {
            return format!("dir_entry failed failed - {:?}",err);
        })?;
        let pathbuf = dir_entry.path();
        if pathbuf.is_file() {
            file_pathbuf_vector.push(pathbuf);
        }else if pathbuf.is_dir(){
            file_pathbuf_vector = iterate_directory_gathering_filenames(&pathbuf,file_pathbuf_vector)?;
        }else{
            return Err(format!("{} is neither file nor dir",&pathbuf.display()));
        }
    }
    return Ok(file_pathbuf_vector);
}
