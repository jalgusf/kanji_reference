#![forbid(unsafe_code)]
#![warn(trivial_casts,trivial_numeric_casts,missing_debug_implementations)]
mod file_chars;

fn main() -> Result<(), u8>{
    let ced = std::env::current_exe().map_err(|err| {
        eprintln!("{}",err);
        return 1;
    })?;
    println!("ced in main.rs is {}",ced.display());

    let ced_dir = ced.parent().ok_or_else(|| {
        eprintln!("can not find directory current exe is running in");
        return 1;
    })?;
    println!("ced_dir in main.rs is {}",ced_dir.display());
    
    std::env::set_current_dir(&ced_dir).map_err(|err| {
        eprintln!("can not set cwd to directory current exe is running in - {:?}", err);
        return 1;
    })?;
    
    let irrelevant_chars = file_chars::irrelevant_chars::get_irrelevant_chars().map_err(|err| {
        eprintln!("Could not get irrelevant chars - {}",err);
        return 1;
    })?;
    
    let reference_chars = file_chars::reference_chars::get_reference_chars().map_err(|err| {
        eprintln!("Could not get reference chars - {}",err);
        return 1;
    })?;

    let mut char_vec = Vec::new();
    for char in reference_chars.into_iter() {
        if !irrelevant_chars.contains(&char) && !char_vec.contains(&char) {
            char_vec.push(char);
        }
    }

    println!("{:?}",char_vec);
    return Ok(())
}
