fn get_irrelevant_chars_in_file<'a>(filepath: &'a std::path::Path) -> Result<std::vec::Vec<char>, String>{
    use std::io::BufRead;
    let file = std::fs::File::open(filepath).map_err(|err| {
        return format!("Could not open file for path {} - {:?}",filepath.display(),err);
    })?;
    let reader = std::io::BufReader::new(file);
    
    let mut char_vec = Vec::new();
    for line_res in reader.lines() {
        let line = line_res.map_err(|err| {
            return format!("We have a line error in file with path {} - {:?}", filepath.display(),err);
        })?;
        for char in line.chars() {
            char_vec.push(char);
        }
    }
    return Ok(char_vec);
}

pub fn get_irrelevant_chars() -> Result<std::vec::Vec<char>, String>{
    let files_path = std::path::Path::new("files/irrelevant_chars/");
    let mut filepath_bufs: Vec<std::path::PathBuf> = Vec::new();
    filepath_bufs = walk_dir::iterate_directory_gathering_filenames(&files_path,filepath_bufs).map_err(|err| {
        return format!("can not get files in files/irrelevant_chars - {:?}", err);
    })?;
    
    let mut chars_complete: std::vec::Vec<char> = Vec::new();
    let mut thread_join_handles: std::vec::Vec<std::thread::JoinHandle<std::vec::Vec<char>>> = Vec::new();
    for filepath_buf in filepath_bufs.into_iter() {
        let join_handle = std::thread::Builder::new().name(format!("thread for file {}",filepath_buf.display())).spawn(move || {
            //println!("thread for file {} is running",filepath_buf.display());
            let res = get_irrelevant_chars_in_file(filepath_buf.as_path());
            //println!("thread for file {} is now returning",filepath_buf.display());
            return res.unwrap();
        }).map_err(|err| {
            return format!("Failed to open thread - {:?}",err);
        })?;
        thread_join_handles.push(join_handle);
    }
    for thread_join_handle in thread_join_handles.into_iter() {
        let mut chars_in_file = thread_join_handle.join().map_err(|err| {
            return format!("One irrelevant_chars thread failed - {:?}",err);
        })?;
        chars_complete.append(&mut chars_in_file);
    }
    if chars_complete.len() == 0 {
        return Err(String::from("Character Vector empty. What went wrong?"));
    }
    return Ok(chars_complete);
}
