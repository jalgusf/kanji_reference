fn get_reference_chars_in_file<'a>(filepath: &'a std::path::Path) -> Result<std::vec::Vec<char>, String>{
    use std::io::BufRead;
    let file = std::fs::File::open(filepath).map_err(|err| {
        return format!("Could not open file for path {} - {:?}",filepath.display(),err);
    })?;
    let reader = std::io::BufReader::new(file);
    
    let mut char_vec = Vec::new();
    let mut headers_passed = false;
    for (index, line_res) in reader.lines().enumerate() {
        let line = line_res.map_err(|err| {
            return format!("We have a line error in file with path {0} at line index {1} - {2:?}", filepath.display(),index,err);
        })?;
        
        if line.is_empty() {
            headers_passed = true;
            continue;
        }
        if headers_passed {
            //println!("line {0} in {1} is {2}",index,filepath.display(),&line);
            for char in line.chars() {
                char_vec.push(char);
            }
        }
    }
    return Ok(char_vec);
}

pub fn get_reference_chars() -> Result<std::vec::Vec<char>, String>{
    let files_path = std::path::Path::new("files/references/");
    let mut filepath_bufs: Vec<std::path::PathBuf> = Vec::new();
    filepath_bufs = walk_dir::iterate_directory_gathering_filenames(&files_path,filepath_bufs).map_err(|err| {
        return format!("can not get files in {} - {:?}", files_path.display(), err);
    })?;
    
    let mut char_vec = Vec::new();
    for filepath_buf in filepath_bufs.into_iter() {
        let chars = get_reference_chars_in_file(filepath_buf.as_path())?;
        for char in chars.into_iter() {
            char_vec.push(char);
        }
    }
    if char_vec.len() == 0 {
        return Err(String::from("Character Vector empty. What went wrong?"));
    }
    return Ok(char_vec);
}
