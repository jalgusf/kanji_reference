This project seeks to provide people learning Japanese with a wealth of references for Kanji.
There already exists a plethora of Kanji dictionaries, which include magnificent details including a number of examples with reading, stroke order, etc.
What I have found missing is references for use of Kanji "in the wild".
This project provides a mechanism for adding references simply by dropping files of a certain format in the specified folder.

Some useful links:
https://www.wadoku.de/search/%E4%BA%BA
https://mpi-lingweb.shh.mpg.de/kanji/
https://www.aozora.gr.jp/