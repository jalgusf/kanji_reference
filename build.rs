fn copy_files(manifest_dir: &str, target_path: &std::path::Path) -> Result<(),u8>{
    let files_path = std::path::Path::new(manifest_dir).join("files");
    let mut file_pathbuf_vector: Vec<std::path::PathBuf> = Vec::new();
    file_pathbuf_vector = walk_dir::iterate_directory_gathering_filenames(&files_path,file_pathbuf_vector).map_err(|err| {
        eprintln!("{}",&err);
        return 1;
    })?;
    
    for file_path in file_pathbuf_vector.into_iter() {
        let mut relative_target_path = std::path::PathBuf::new();
        let mut base_found = false;
        let mut base_path = std::path::PathBuf::new();
        for path_part in file_path.iter() {
            if base_found {
                relative_target_path = relative_target_path.join(path_part);
            }else{
                base_path = base_path.join(path_part);
                if files_path == base_path {
                    base_found = true;
                    relative_target_path = relative_target_path.join(path_part);
                }
            }
        }
        let target_path_complete = target_path.join(&relative_target_path);
        let target_path_parent_opt = target_path_complete.parent();
        if target_path_parent_opt.is_some() {
            std::fs::create_dir_all(target_path_parent_opt.unwrap()).map_err(|err| {
                eprintln!("could not create path {} - {:?}",target_path_parent_opt.unwrap().display(),err);
                return 2;
            })?;
        }
        std::fs::copy(&file_path,&target_path.join(&relative_target_path)).map_err(|err| {
            eprintln!("could not copy {0} to {1} - {2:?}",file_path.display(),target_path.join(&relative_target_path).display(), err);
            return 2;
        })?;
    }
    return Ok(());
}

fn main() -> Result<(),u8>{
    let manifest_dir = std::env::var("CARGO_MANIFEST_DIR").map_err(|err| {
        eprintln!("could not get manifest_dir - {:?}", err);
        return 1;
    })?;
    println!("manifest_dir is {}",&manifest_dir);
    
    let profile = std::env::var("PROFILE").map_err(|err| {
        eprintln!("could not get profile - {:?}", err);
        return 1;
    })?;
    println!("profile is {}",&profile);
    
    let target_path = std::path::Path::new(&manifest_dir).join("target").join(&profile);
    println!("target_path is {}",&target_path.display());
    
    copy_files(&manifest_dir,&target_path)?;
    return Ok(());
}
